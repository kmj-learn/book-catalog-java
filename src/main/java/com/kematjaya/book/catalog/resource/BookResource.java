package com.kematjaya.book.catalog.resource;

import com.kematjaya.book.catalog.dto.BookCreateRequestDTO;
import com.kematjaya.book.catalog.dto.BookUpdateRequestDTO;
import com.kematjaya.book.catalog.entity.Book;
import com.kematjaya.book.catalog.service.BookServiceInterface;
import java.util.Map;
import javax.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/book")
public class BookResource {
    
    private BookServiceInterface bookService;
    
    // api/book/?page=0&size=10&sort=name,asc&q=a
    @GetMapping("/")
    public @ResponseBody Iterable<Book> index(@RequestParam Map<String,String> params, Pageable pageable) {
        
        return bookService.list(params, pageable);
    }
    
    @PostMapping("/create")
    public @ResponseBody Book create(@RequestBody @Valid BookCreateRequestDTO dto) {
        
        return bookService.create(dto);
    }
    
    @PutMapping("/{id}")
    public @ResponseBody Book update(@PathVariable Long id, @RequestBody @Valid BookUpdateRequestDTO dto) {
        return bookService.update(id, dto);
    }
    
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        bookService.delete(id);
        
        return ResponseEntity.ok().build();
    }
}
