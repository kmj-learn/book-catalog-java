package com.kematjaya.book.catalog.resource;

import com.kematjaya.book.catalog.dto.AuthorCreateRequestDTO;
import com.kematjaya.book.catalog.dto.AuthorUpdateRequestDTO;
import com.kematjaya.book.catalog.entity.Author;
import com.kematjaya.book.catalog.service.AuthorServiceInterface;
import java.util.Optional;
import javax.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/author")
public class AuthorResource {

    private AuthorServiceInterface authorService;
    
    // api/author/?page=0&size=10&sort=name,asc&q=a
    @GetMapping("/")
    public @ResponseBody Iterable<Author> index(@RequestParam("q") Optional<String> q, Pageable pageable) {
        String query = q.orElse(null);
        
        return authorService.list(query, pageable);
    }
    
    @PostMapping("/create")
    public @ResponseBody Author create(@RequestBody @Valid AuthorCreateRequestDTO dto) {
        return authorService.create(dto);
    }
    
    @PutMapping("/{id}")
    public @ResponseBody Author update(@PathVariable Long id, @RequestBody @Valid AuthorUpdateRequestDTO dto) {
        return authorService.update(id, dto);
    }
    
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        authorService.delete(id);
        
        return ResponseEntity.ok().build();
    }
}
