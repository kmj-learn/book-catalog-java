package com.kematjaya.book.catalog.resource;

import com.kematjaya.book.catalog.entity.Category;
import com.kematjaya.book.catalog.dto.CategoryCreateRequestDTO;
import com.kematjaya.book.catalog.dto.CategoryUpdateRequestDTO;
import com.kematjaya.book.catalog.service.CategoryServiceInterface;
import java.util.Optional;
import javax.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/category")
public class CategoryResource {

    private CategoryServiceInterface categoryService;
    
    // api/category/?page=0&size=10&sort=name,asc&q=a
    @GetMapping("/")
    public @ResponseBody Iterable<Category> index(@RequestParam("q") Optional<String> q, Pageable pageable) {
        String query = q.orElse(null);
        return categoryService.list(query, pageable);
    }
    
    @PostMapping("/create")
    public @ResponseBody Category create(@RequestBody @Valid CategoryCreateRequestDTO dto) {
        Category category = categoryService.create(dto);
        
        return category;
    }
    
    @PutMapping("/{id}")
    public @ResponseBody Category update(@PathVariable Long id, @RequestBody @Valid CategoryUpdateRequestDTO dto) {
        return categoryService.update(id, dto);
    }
    
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        categoryService.delete(id);
        
        return ResponseEntity.ok().build();
    }
}
