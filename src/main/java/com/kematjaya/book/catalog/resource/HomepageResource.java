package com.kematjaya.book.catalog.resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
@Controller
public class HomepageResource {

    @RequestMapping("/")
    public String index() {
        return "homepage";
    }
}
