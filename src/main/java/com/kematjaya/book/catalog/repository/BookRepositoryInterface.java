package com.kematjaya.book.catalog.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.kematjaya.book.catalog.entity.Book;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
@Repository
public interface BookRepositoryInterface extends JpaRepository<Book, Long>{

    Page<Book> findByIsbnContainingAndTitleContaining(String isbn, String title, Pageable pageable);
    
    Page<Book> findByIsbnContaining(String isbn, Pageable pageable);
    
    Page<Book> findByTitleContaining(String title, Pageable pageable);
}
