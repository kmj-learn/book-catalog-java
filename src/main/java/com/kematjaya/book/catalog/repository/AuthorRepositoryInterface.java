package com.kematjaya.book.catalog.repository;

import com.kematjaya.book.catalog.entity.Author;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
@Repository
public interface AuthorRepositoryInterface extends JpaRepository<Author, Long> {

    @Query("select t from Author t where lower(t.name) like lower(concat('%', ?1,'%'))")
    Page<Author> findByNameLike(String name, Pageable pageable);
    
}
