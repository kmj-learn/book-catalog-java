package com.kematjaya.book.catalog.repository;

import com.kematjaya.book.catalog.entity.Category;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
@Repository
public interface CategoryRepositoryInterface extends JpaRepository<Category, Long>{

    @Query("select t from Category t where lower(t.name) like lower(concat('%', ?1,'%'))")
    Page<Category> findByNameLike(String name, Pageable pageable);
}
