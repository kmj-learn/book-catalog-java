package com.kematjaya.book.catalog.service;

import com.kematjaya.book.catalog.dto.BookCreateRequestDTO;
import com.kematjaya.book.catalog.dto.BookUpdateRequestDTO;
import com.kematjaya.book.catalog.entity.Book;
import java.util.Map;
import org.springframework.data.domain.Pageable;

/**
 *
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
public interface BookServiceInterface {

    public Iterable<Book> list(Map<String,String> queries, Pageable pageable);
    
    public Book create(BookCreateRequestDTO bookCreateRequestDTO);
    
    public Book update(Long id, BookUpdateRequestDTO bookUpdateRequestDTO);
    
    public Boolean delete(Long id);
}
