package com.kematjaya.book.catalog.service;

import com.kematjaya.book.catalog.dto.CategoryCreateRequestDTO;
import com.kematjaya.book.catalog.dto.CategoryUpdateRequestDTO;
import com.kematjaya.book.catalog.entity.Category;
import org.springframework.data.domain.Pageable;

/**
 *
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
public interface CategoryServiceInterface {

    public Iterable<Category> list(String name, Pageable pageable);
    
    public Category create(CategoryCreateRequestDTO categoryCreateRequestDTO);
    
    public Category update(Long id, CategoryUpdateRequestDTO categoryUpdateRequestDTO);
    
    public Boolean delete(Long id);
}
