package com.kematjaya.book.catalog.service;

import com.kematjaya.book.catalog.dto.CategoryCreateRequestDTO;
import com.kematjaya.book.catalog.dto.CategoryUpdateRequestDTO;
import com.kematjaya.book.catalog.entity.Category;
import com.kematjaya.book.catalog.exception.BadRequestException;
import com.kematjaya.book.catalog.repository.CategoryRepositoryInterface;
import java.util.Optional;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
@Component
@AllArgsConstructor
public class CategoryService implements CategoryServiceInterface {

    private CategoryRepositoryInterface categoryRepository;
    
    @Override
    public Category create(CategoryCreateRequestDTO categoryCreateRequestDTO) {
        Category category = new Category();
        category.setName(categoryCreateRequestDTO.getName());
        
        categoryRepository.save(category);
        
        return category;
    }

    @Override
    public Category update(Long id, CategoryUpdateRequestDTO categoryUpdateRequestDTO) {
        Category category = categoryRepository.findById(id)
                .orElseThrow(()-> new BadRequestException("category not found"));
        category.setName(categoryUpdateRequestDTO.getName());
        
        categoryRepository.save(category);
        
        return category;
    }

    @Override
    public Boolean delete(Long id) {
        Category category = categoryRepository.findById(id)
                .orElseThrow(()-> new BadRequestException("category not found"));
        
        categoryRepository.delete(category);
        
        return true;
    }

    @Override
    public Iterable<Category> list(String name, Pageable pageable) {
        if (null != name) {
            return categoryRepository.findByNameLike(name, pageable);
        }
        
        return categoryRepository.findAll(pageable);
    }

}
