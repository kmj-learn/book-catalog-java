package com.kematjaya.book.catalog.service;

import com.kematjaya.book.catalog.dto.AuthorCreateRequestDTO;
import com.kematjaya.book.catalog.dto.AuthorUpdateRequestDTO;
import com.kematjaya.book.catalog.entity.Author;
import org.springframework.data.domain.Pageable;

/**
 *
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
public interface AuthorServiceInterface {
    
    public Iterable<Author> list(String name, Pageable pageable);
    
    public Author create(AuthorCreateRequestDTO authorCreateRequestDTO);
    
    public Author update(Long id, AuthorUpdateRequestDTO authorUpdateRequestDTO);
    
    public Boolean delete(Long id);
}
