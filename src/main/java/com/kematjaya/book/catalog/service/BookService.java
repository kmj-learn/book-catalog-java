package com.kematjaya.book.catalog.service;

import java.util.List;
import com.kematjaya.book.catalog.dto.BookCreateRequestDTO;
import com.kematjaya.book.catalog.dto.BookUpdateRequestDTO;
import com.kematjaya.book.catalog.entity.Book;
import com.kematjaya.book.catalog.entity.Author;
import com.kematjaya.book.catalog.entity.Category;
import com.kematjaya.book.catalog.exception.BadRequestException;
import com.kematjaya.book.catalog.repository.AuthorRepositoryInterface;
import com.kematjaya.book.catalog.repository.BookRepositoryInterface;
import com.kematjaya.book.catalog.repository.CategoryRepositoryInterface;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

/**
 *
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
@Component
@AllArgsConstructor
public class BookService implements BookServiceInterface {

    private BookRepositoryInterface bookRepository;
    
    private AuthorRepositoryInterface authorRepository;
    
    private CategoryRepositoryInterface categoryRepository;
    
    @Override
    public Iterable<Book> list(Map<String, String> queries, Pageable pageable) {
        String isbn = queries.get("isbn");
        String title = queries.get("title");
        if (null != isbn && null != title) {
            return bookRepository.findByIsbnContainingAndTitleContaining(isbn, title, pageable);
        }
        
        if (null != isbn) {
            return bookRepository.findByIsbnContaining(isbn, pageable);
        }
        
        if (null != title) {
            return bookRepository.findByTitleContaining(title, pageable);
        }
        
        return bookRepository.findAll(pageable);
    }

    @Override
    public Book create(BookCreateRequestDTO bookCreateRequestDTO) {
        Author author = authorRepository.findById(bookCreateRequestDTO.getAuthorId())
                .orElseThrow(()->new BadRequestException("invalid author id"));
        
        Book book = new Book();
        if (null != bookCreateRequestDTO.getCategoryId()) {
            List<Category> listCategories = categoryRepository.findAllById(bookCreateRequestDTO.getCategoryId());
            Set<Category> categories =  new HashSet<>(listCategories);
            
            book.setCategories(categories);
        }
        
        book.setIsbn(bookCreateRequestDTO.getIsbn());
        book.setTitle(bookCreateRequestDTO.getTitle());
        book.setAuthor(author);
        
        bookRepository.save(book);
        
        return book;
    }

    @Override
    public Book update(Long id, BookUpdateRequestDTO bookUpdateRequestDTO) {
        Book book = bookRepository.findById(id)
                .orElseThrow(()-> new BadRequestException("book not found"));
        
        if (null != bookUpdateRequestDTO.getCategoryId()) {
            List<Category> listCategories = categoryRepository.findAllById(bookUpdateRequestDTO.getCategoryId());
            Set<Category> categories =  new HashSet<>(listCategories);
            
            book.setCategories(categories);
        }
        
        if (null != bookUpdateRequestDTO.getAuthorId()) {
            Author author = authorRepository.findById(bookUpdateRequestDTO.getAuthorId())
                .orElseThrow(()->new BadRequestException("invalid author id"));
            book.setAuthor(author);
        }
        
        book.setIsbn(null != bookUpdateRequestDTO.getIsbn() ? bookUpdateRequestDTO.getIsbn():book.getIsbn());
        book.setTitle(null != bookUpdateRequestDTO.getTitle() ? bookUpdateRequestDTO.getTitle():book.getTitle());
        
        bookRepository.save(book);
        
        return book;
    }

    @Override
    public Boolean delete(Long id) {
        Book book = bookRepository.findById(id)
                .orElseThrow(()-> new BadRequestException("book not found"));
        
        bookRepository.delete(book);
        
        return true;
    }

}
