package com.kematjaya.book.catalog.service;

import com.kematjaya.book.catalog.dto.AuthorCreateRequestDTO;
import com.kematjaya.book.catalog.dto.AuthorUpdateRequestDTO;
import com.kematjaya.book.catalog.entity.Author;
import com.kematjaya.book.catalog.exception.BadRequestException;
import com.kematjaya.book.catalog.repository.AuthorRepositoryInterface;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

/**
 *
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
@Component
@AllArgsConstructor
public class AuthorService implements  AuthorServiceInterface {

    private AuthorRepositoryInterface authorRepository;
            
    @Override
    public Iterable<Author> list(String name, Pageable pageable) {
        if (null != name) {
            return authorRepository.findByNameLike(name, pageable);
        }
        
        return authorRepository.findAll(pageable);
    }

    @Override
    public Author create(AuthorCreateRequestDTO authorCreateRequestDTO) {
        Author author = new Author();
        author.setName(authorCreateRequestDTO.getName());
        author.setBirthDate(authorCreateRequestDTO.getBirthDate());
        
        authorRepository.save(author);
        
        return author;
    }

    @Override
    public Author update(Long id, AuthorUpdateRequestDTO authorUpdateRequestDTO) {
        Author author = authorRepository.findById(id)
                .orElseThrow(()-> new BadRequestException("author tidak ditemukan."));
        
        author.setName(null != authorUpdateRequestDTO.getName() ? authorUpdateRequestDTO.getName() : author.getName());
        author.setBirthDate(null != authorUpdateRequestDTO.getBirthDate() ? authorUpdateRequestDTO.getBirthDate() : author.getBirthDate());
        
        authorRepository.save(author);
        
        return author;
    }

    @Override
    public Boolean delete(Long id) {
        Author author = authorRepository.findById(id)
                .orElseThrow(()-> new BadRequestException("author not found"));
        
        authorRepository.delete(author);
        
        return true;
    }

}
