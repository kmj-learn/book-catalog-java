package com.kematjaya.book.catalog.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import java.util.List;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import lombok.Data;

/**
 *
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
@Data
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class BookCreateRequestDTO {

    @NotBlank
    private String isbn;
    
    @NotBlank
    private String title;
    
    @NotBlank
    private Long authorId;
    
    @NotEmpty
    private List<Long> categoryId;
}
