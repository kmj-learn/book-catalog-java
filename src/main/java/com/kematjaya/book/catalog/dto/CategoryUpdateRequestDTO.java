package com.kematjaya.book.catalog.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import javax.validation.constraints.NotBlank;
import lombok.Data;

/**
 *
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
@Data
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class CategoryUpdateRequestDTO {

    private static final Long serialVersionUID = 123456799L;
    
    @NotBlank
    private String name;
}
