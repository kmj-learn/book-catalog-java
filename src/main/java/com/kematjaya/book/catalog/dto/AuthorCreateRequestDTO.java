package com.kematjaya.book.catalog.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import java.time.LocalDate;
import javax.validation.constraints.NotBlank;
import lombok.Data;

/**
 *
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
@Data
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class AuthorCreateRequestDTO {
    
    private static final Long serialVersionUID = 123456789L;
    
    @NotBlank
    private String name;
    
    @NotBlank
    private LocalDate birthDate;
}
