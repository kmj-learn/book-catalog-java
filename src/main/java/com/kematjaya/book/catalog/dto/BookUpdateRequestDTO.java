package com.kematjaya.book.catalog.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import java.util.List;
import lombok.Data;

/**
 *
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
@Data
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class BookUpdateRequestDTO {

    private String isbn;
    
    private String title;
    
    private Long authorId;
    
    private List<Long> categoryId;
}
