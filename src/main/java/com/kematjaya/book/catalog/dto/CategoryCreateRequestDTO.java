package com.kematjaya.book.catalog.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import javax.validation.constraints.NotBlank;
import lombok.Data;

/**
 *
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
@Data
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class CategoryCreateRequestDTO {

    private static final Long serialVersionUID = 123456789L;
    
    @NotBlank
    private String name;
}
